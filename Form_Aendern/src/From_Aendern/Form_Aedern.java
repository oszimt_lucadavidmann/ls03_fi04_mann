package From_Aendern;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Form_Aedern {

	private JFrame frame;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_Aedern window = new Form_Aedern();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Form_Aedern() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	JPanel background = new JPanel();
	
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 408, 625);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 21, 382, 14);
		background.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblNewLabel_1.setBounds(10, 60, 226, 14);
		background.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Aufgabe 2: Text Formatieren");
		lblNewLabel_2.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(10, 179, 160, 14);
		background.add(lblNewLabel_2);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(10, 237, 382, 20);
		background.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnNewButton_3_1_3 = new JButton("Ins Label schreiben");
		btnNewButton_3_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(txtHierBitteText.getText());
			}
		});
		btnNewButton_3_1_3.setFont(new Font("Arial", Font.PLAIN, 12));
		btnNewButton_3_1_3.setBounds(10, 268, 191, 23);
		background.add(btnNewButton_3_1_3);
		
		JButton btnNewButton_3_1_4 = new JButton("Text im Label l\u00F6schen");
		btnNewButton_3_1_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");
			}
		});
		btnNewButton_3_1_4.setFont(new Font("Arial", Font.PLAIN, 12));
		btnNewButton_3_1_4.setBounds(201, 268, 191, 23);
		background.add(btnNewButton_3_1_4);
		
		JLabel lblAufgabeSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeSchriftfarbe.setFont(new Font("Arial", Font.PLAIN, 12));
		lblAufgabeSchriftfarbe.setBounds(10, 302, 168, 14);
		background.add(lblAufgabeSchriftfarbe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.RED);
			}
		});
		btnRot_1.setFont(new Font("Arial", Font.PLAIN, 12));
		btnRot_1.setBounds(10, 86, 121, 23);
		background.add(btnRot_1);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setFont(new Font("Arial", Font.PLAIN, 12));
		btnGelb.setBounds(10, 119, 121, 23);
		background.add(btnGelb);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.GREEN);
			}
		});
		btnGrn.setFont(new Font("Arial", Font.PLAIN, 12));
		btnGrn.setBounds(141, 86, 121, 23);
		background.add(btnGrn);
		
		JButton btnStandartfarbe = new JButton("Standartfarbe");
		btnStandartfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				background.setBackground(new Color(0xEEEEEE));
			}
		});
		btnStandartfarbe.setFont(new Font("Arial", Font.PLAIN, 12));
		btnStandartfarbe.setBounds(141, 119, 121, 23);
		background.add(btnStandartfarbe);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				background.setBackground(Color.BLUE);
			}
		});
		btnBlau_1.setFont(new Font("Arial", Font.PLAIN, 12));
		btnBlau_1.setBounds(271, 86, 121, 23);
		background.add(btnBlau_1);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				background.setBackground(JColorChooser.showDialog(null,"bitte Farbewählen", Color.RED));
			}
		});
		btnFarbeWhlen.setFont(new Font("Arial", Font.PLAIN, 12));
		btnFarbeWhlen.setBounds(272, 119, 121, 23);
		background.add(btnFarbeWhlen);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnArial.setFont(new Font("Arial", Font.PLAIN, 12));
		btnArial.setBounds(10, 204, 121, 23);
		background.add(btnArial);
		
		JButton button_7 = new JButton("Comic Sans MS");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		button_7.setFont(new Font("Arial", Font.PLAIN, 12));
		button_7.setBounds(141, 204, 121, 23);
		background.add(button_7);
		
		JButton btnCourerNew = new JButton("Courier New");
		btnCourerNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourerNew.setFont(new Font("Arial", Font.PLAIN, 12));
		btnCourerNew.setBounds(271, 204, 121, 23);
		background.add(btnCourerNew);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setFont(new Font("Arial", Font.PLAIN, 12));
		lblAufgabeSchriftgre.setBounds(10, 361, 208, 14);
		background.add(lblAufgabeSchriftgre);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String font = lblNewLabel.getFont().getFontName();
				int groesse = lblNewLabel.getFont().getSize();
				lblNewLabel.setFont(new Font(font, Font.PLAIN, groesse + 1));
			}
		});
		button.setFont(new Font("Arial", Font.PLAIN, 12));
		button.setBounds(10, 386, 191, 23);
		background.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String font = lblNewLabel.getFont().getFontName();
				int groesse = lblNewLabel.getFont().getSize();
				lblNewLabel.setFont(new Font(font, Font.PLAIN, groesse - 1));
			}
		});
		button_1.setFont(new Font("Arial", Font.PLAIN, 12));
		button_1.setBounds(211, 386, 181, 23);
		background.add(button_1);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		btnRot.setFont(new Font("Arial", Font.PLAIN, 12));
		btnRot.setBounds(10, 327, 121, 23);
		background.add(btnRot);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLUE);
			}
		});
		btnBlau.setFont(new Font("Arial", Font.PLAIN, 12));
		btnBlau.setBounds(141, 327, 121, 23);
		background.add(btnBlau);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setFont(new Font("Arial", Font.PLAIN, 12));
		btnSchwarz.setBounds(271, 328, 121, 23);
		background.add(btnSchwarz);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setFont(new Font("Arial", Font.PLAIN, 12));
		lblAufgabeTextausrichtung.setBounds(10, 420, 168, 14);
		background.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setFont(new Font("Arial", Font.PLAIN, 12));
		btnLinksbndig.setBounds(10, 441, 121, 23);
		background.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setFont(new Font("Arial", Font.PLAIN, 12));
		btnZentriert.setBounds(141, 442, 121, 23);
		background.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setFont(new Font("Arial", Font.PLAIN, 12));
		btnRechtsbndig.setBounds(271, 442, 121, 23);
		background.add(btnRechtsbndig);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setFont(new Font("Arial", Font.PLAIN, 12));
		lblAufgabeProgramm.setBounds(10, 475, 181, 14);
		background.add(lblAufgabeProgramm);
		
		JButton btnNewButton = new JButton("Exit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton.setFont(new Font("Arial", Font.PLAIN, 12));
		btnNewButton.setBounds(0, 500, 392, 93);
		background.add(btnNewButton);
		
		
		background.setBounds(0, 0, 392, 593);
		frame.getContentPane().add(background);
		background.setLayout(null);
		
	}
}
