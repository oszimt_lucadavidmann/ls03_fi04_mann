package view.menue;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.UserController;
import model.User;

//TODO create a usermanagement
public class CreateUserWindow extends JFrame {
	private JTextField tfSN;
	private JTextField tfFN;
	private JTextField tfBD;
	private JTextField tfST;
	private JTextField tfHN;
	private JTextField tfMS;
	private JTextField tfPY;
	private JTextField tfCT;
	private JTextField tfPC;
	private JTextField tfLN;
	private JTextField tfPW;
	private JTextField tfFG;
private UserController controller;

public CreateUserWindow(UserController controller) {
		        this.controller = controller;
		Container panel = new JPanel();
		
		
		panel.setBackground(Color.WHITE);
		getContentPane().setLayout(null);
		setSize(400,430);
		
		JLabel lbFN = new JLabel("First Name:");
		lbFN.setBounds(10, 21, 139, 14);
		getContentPane().add(lbFN);
		
		JLabel lblSN = new JLabel("Sur Name:");
		lblSN.setBounds(10, 46, 139, 14);
		getContentPane().add(lblSN);
		
		JLabel lblBD = new JLabel("Birthday:");
		lblBD.setBounds(10, 71, 139, 14);
		getContentPane().add(lblBD);
		
		JLabel lblST = new JLabel("Street:");
		lblST.setBounds(10, 96, 139, 14);
		getContentPane().add(lblST);
		
		JLabel lblHN = new JLabel("House Number:");
		lblHN.setBounds(10, 121, 139, 14);
		getContentPane().add(lblHN);
		
		JLabel lblPC = new JLabel("Postal Code:");
		lblPC.setBounds(10, 146, 139, 14);
		getContentPane().add(lblPC);
		
		JLabel lblCT = new JLabel("City:");
		lblCT.setBounds(10, 171, 139, 14);
		getContentPane().add(lblCT);
		
		JLabel lblLN = new JLabel("Login Name:");
		lblLN.setBounds(10, 196, 139, 14);
		getContentPane().add(lblLN);
		
		JLabel lblPW = new JLabel("Password:");
		lblPW.setBounds(10, 221, 139, 14);
		getContentPane().add(lblPW);
		
		JLabel lblPY = new JLabel("Payment:");
		lblPY.setBounds(10, 246, 139, 14);
		getContentPane().add(lblPY);
		
		JLabel lblMS = new JLabel("marital status:");
		lblMS.setBounds(10, 271, 139, 14);
		getContentPane().add(lblMS);
		
		JLabel lblFG = new JLabel("Final Grade:");
		lblFG.setBounds(10, 296, 139, 14);
		getContentPane().add(lblFG);
		
		tfFN = new JTextField();
		tfFN.setBounds(188, 18, 141, 20);
		getContentPane().add(tfFN);
		tfFN.setColumns(10);
		
		tfSN = new JTextField();
		tfSN.setBounds(188, 43, 141, 20);
		getContentPane().add(tfSN);
		tfSN.setColumns(10);
		
		tfBD = new JTextField();
		tfBD.setBounds(188, 68, 141, 20);
		getContentPane().add(tfBD);
		tfBD.setColumns(10);
		
		tfST = new JTextField();
		tfST.setBounds(188, 93, 141, 20);
		getContentPane().add(tfST);
		tfST.setColumns(10);
		
		tfHN = new JTextField();
		tfHN.setBounds(188, 118, 141, 20);
		getContentPane().add(tfHN);
		tfHN.setColumns(10);
		
		tfPC = new JTextField();
		tfPC.setBounds(188, 143, 141, 20);
		getContentPane().add(tfPC);
		tfPC.setColumns(10);
		
		tfCT = new JTextField();
		tfCT.setBounds(188, 168, 141, 20);
		getContentPane().add(tfCT);
		tfCT.setColumns(10);
		
		tfLN = new JTextField();
		tfLN.setBounds(188, 193, 141, 20);
		getContentPane().add(tfLN);
		tfLN.setColumns(10);
		
		tfPW = new JTextField();
		tfPW.setBounds(188, 218, 141, 20);
		getContentPane().add(tfPW);
		tfPW.setColumns(10);
		
		tfPY = new JTextField();
		tfPY.setBounds(188, 243, 141, 20);
		getContentPane().add(tfPY);
		tfPY.setColumns(10);
		
		tfMS = new JTextField();
		tfMS.setBounds(188, 268, 141, 20);
		getContentPane().add(tfMS);
		tfMS.setColumns(10);
		
		tfFG = new JTextField();
		tfFG.setBounds(188, 293, 141, 20);
		getContentPane().add(tfFG);
		tfFG.setColumns(10);
		
		JButton btnCreateUser = new JButton("Create User");
		btnCreateUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	            LocalDate localDate = LocalDate.parse(tfBD.getText(), formatter);
	            
	            User user = new User(
	                    -1, 
	                    tfFN.getText(), 
	                    tfSN.getText(), 
	                    localDate, 
	                    tfST.getText(), 
	                    tfHN.getText(), 
	                    tfPC.getText(), 
	                    tfCT.getText(), 
	                    tfLN.getText(), 
	                    tfPW.getText(), 
	                    Integer.parseInt(tfPY.getText()), 
	                    tfMS.getText(), 
	                    Integer.parseInt(tfFG.getText())
	            );
	            controller.createUser(user);
	            System.out.println("The user " + user.getLoginname() + " was created.");
			}
		});
		btnCreateUser.setBounds(0, 349, 364, 32);
		getContentPane().add(btnCreateUser);
		
		JLabel lblLegenSieEinen = new JLabel("Please crate a new user");
		lblLegenSieEinen.setHorizontalAlignment(SwingConstants.CENTER);
		lblLegenSieEinen.setBounds(10, 0, 235, 14);
		getContentPane().add(lblLegenSieEinen);
	}

	private void clearInputs() {
		
	}

}
