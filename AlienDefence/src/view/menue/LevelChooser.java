package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private User user;
	private AlienDefenceController alienDefenceController;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user, String mode) {
		setBackground(Color.BLACK);
		setForeground(Color.BLACK);
		this.alienDefenceController = alienDefenceController;
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;
		this.user = user;

		setLayout(new BorderLayout());

		
		
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		pnlButtons.setForeground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

			JButton btnNewLevel = new JButton("Neues Level");
			btnNewLevel.setForeground(Color.BLACK);
			btnNewLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnNewLevel_Clicked();
				}
			});
			pnlButtons.add(btnNewLevel);

			JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
			btnUpdateLevel.setForeground(Color.BLACK);
			btnUpdateLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnUpdateLevel_Clicked();
				}
			});
			pnlButtons.add(btnUpdateLevel);

			JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
			btnDeleteLevel.setForeground(Color.BLACK);
			btnDeleteLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnDeleteLevel_Clicked();
				}
			});
			pnlButtons.add(btnDeleteLevel);
		
		

		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.setForeground(Color.BLACK);
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTestLevel_Start();
			}
		});
		
		pnlButtons.add(btnSpielen);
		
		JButton btnHighscore = new JButton("High Score");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHighscore_ShowScore(alienDefenceController);			
			}
		});
		pnlButtons.add(btnHighscore);
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
		
		switch (mode) {
		case "testing":
			btnDeleteLevel.setVisible(false);
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnHighscore.setVisible(false);
			break;
		case "editing":
			btnSpielen.setVisible(false);
		case "highscore":
			btnDeleteLevel.setVisible(false);
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnSpielen.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}

	private void btnTestLevel_Start() {
		Thread t = new Thread("GameThread") {

			@Override
			public void run() {
				List<Level> levels = alienDefenceController.getLevelController().readAllLevels();
				int levelId = Integer.parseInt((String) tblLevels.getModel().getValueAt(tblLevels.getSelectedRow(), 0));

				GameController gameController = alienDefenceController.startGame(levels.get(levelId - 1), user);
				new GameGUI(gameController).start();

			}
		};
		t.start();
		
		this.leveldesignWindow.dispose();
	}
	
	public void btnHighscore_ShowScore(AlienDefenceController alienController) {
        if(tblLevels.getSelectedRow() >= 0) {
            this.leveldesignWindow.dispose();
            
            int levelIndex = Integer.parseInt((String) tblLevels.getModel().getValueAt(tblLevels.getSelectedRow(), 0));
            new Highscore(alienController.getAttemptController(), levelIndex);
        }
    }

}
