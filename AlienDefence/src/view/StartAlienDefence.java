package view;

import controller.AlienDefenceController;
import model.persistance.IPersistance;
import model.persistanceDB.PersistanceDB;
import view.menue.MainMenue;

public class StartAlienDefence {

	public static void main(String[] args) {
		
		IPersistance 		   alienDefenceModel      = new PersistanceDB();
		AlienDefenceController alienDefenceController = new AlienDefenceController(alienDefenceModel);
		MainMenue              mainMenue              = new MainMenue(alienDefenceController);
		
		mainMenue.setVisible(true);
		
//	      UserController controller = new UserController(alienDefenceModel);
//	        EventQueue.invokeLater(() -> {
//	            try {
//	                CreateUserWindow frame = new CreateUserWindow(controller);
//	                frame.setVisible(true);
//	            } catch (Exception e) {
//	                e.printStackTrace();
//	            }
//	        });
	}
}
