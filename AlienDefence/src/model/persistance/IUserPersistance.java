package model.persistance;

import model.User;

public interface IUserPersistance {

	User readUser(String username);
	
	public int createUser(User user);
	
	public void deleteUser(User user);
	
	public void updateUser(User user);
}