package controller;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * 
 * @author Clara Zufall
 */
public class UserController {

	private IUserPersistance userPersistance;

	public UserController(IPersistance persistance) {
		this.userPersistance = persistance.getUserPersistance();
	}

	/**
	 * Nimmt das User Objekt und erstellt daraus ein neuen Nutzer in der Datenbank
	 * 
	 * @param user ist der Nutzer der Angelegt werden soll
	 */
	public void createUser(User user) {
		if (userPersistance.readUser(user.getLoginname()) == null) {
			this.userPersistance.createUser(user);
		}
	}

	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zurück
	 * 
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		return userPersistance.readUser(username);
	}

	/**
	 * Aendert nutzer werte in deer Datenbank
	 * 
	 * @param user ist der Nutzer der überschrieben werden soll
	 */
	public void changeUser(User user) {
		if (userPersistance.readUser(user.getLoginname()) != null) {
			this.userPersistance.updateUser(user);
		}
	}

	/**
	 * Loescht einen nutzer aus der Datenbank
	 * 
	 * @param user Nutzer der geloescht werden soll
	 */
	public void deleteUser(User user) {
		if (userPersistance.readUser(user.getLoginname()) != null) {
			this.userPersistance.deleteUser(user);
		}
	}

	/**
	 * Ueberprueft ob das passwort was im nutzer Objekt steht auch das gleich ist
	 * was uebergeben wird
	 * 
	 * @param username ist der Nutzer der gecheckt wird
	 * @param passwort
	 * @return gibt true oder false zurueck wenn das passwort stimmt oder nicht
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username, passwort);
		return user.getPassword().equals(passwort);
	}

}
